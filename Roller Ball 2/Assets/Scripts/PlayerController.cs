﻿//Player Script:  Controls ball movement and collision detection

//Game Objects:
//tag: A tag can be used to identify a game object. Tags must be declared in the Tags and Layers manager before using them.
//SetActive: Activates/Deactivates the GameObject.
//CompareTag: Is this game object tagged with tag ?

using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerController : MonoBehaviour
{

    public float speed;  // Speed at which the ball rolls

    private Rigidbody rb;  // Shape for the ball.  Rigid is used for dynamic physics calculation.
    private int count;  // keep track of collectibles
    public Text countText; // String to keep track of collectibles 
    public Text winText = null;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        setCountText();


    }

    //Movements for the ball, one for Horizontal (x) one for vertical (z) and 0 for (y)
    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        //Create movement speed for the ball
        rb.AddForce(movement * speed);
    }

    // Collider to detect when the game object touches a "Pick Up" item.  It will cause it to deactivate or disappear.
     void OnTriggerEnter(Collider other)
    {
        //"Pick Up" is the name of the objects the Player will be collecting.
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count += 1; // increment count for picked up item
            setCountText();
        }

    }

    void setCountText()
    {
        countText.text = "Count: " + count.ToString();

        if (count >= 12)
            winText.text = "You Win!";
    }
}
